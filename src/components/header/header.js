import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import {connect} from 'react-redux';

class Header extends React.Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
        
    }
    asideToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('aside-menu-hidden');
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    
    render() {
        return (
            <div>
                <Navbar color="dark" dark expand="md">
                    <Nav className="nav-toggle" navbar>
                        <NavItem>                            
                            <NavLink href="#" onClick={this.asideToggle}><i className="material-icons">menu</i></NavLink>
                        </NavItem>
                    </Nav>
                    <NavbarBrand href="/">WIX</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href="/components/"></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/"></NavLink>
                            </NavItem>
                            <UncontrolledDropdown nav inNavbar>
                                <DropdownToggle nav caret>

                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem>

                                    </DropdownItem>
                                    <DropdownItem>

                                    </DropdownItem>
                                    <DropdownItem divider />
                                    <DropdownItem>

                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

export default connect (null)(Header);