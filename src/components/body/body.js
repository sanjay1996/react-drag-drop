import React, { Component } from 'react';
import {
    Card, CardBody, Row,
    Button, Col,
    Modal, ModalHeader, ModalBody, ModalFooter, Container
} from 'reactstrap';
import '../body/body.css';
import { connect } from 'react-redux';
import { exportHeader, exportBanner,exportFooter } from '../../redux/action/global';
import 'html-to-react';
import { Link } from 'react-router-dom'


import Header1 from '../Selection/header/header';
import Header2 from '../Selection/header/header1';
import Header3 from '../Selection/header/header2';
import Header4 from '../Selection/header/header3';
import Footer1 from '../Selection/footer/footer1';
import Footer2 from '../Selection/footer/footer2';
import Footer3 from '../Selection/footer/footer3';
import Footer4 from '../Selection/footer/footer4';
import Banner from '../Selection/banner/banner';
import Minicarousel from '../Selection/carousel/mini-carousel'
import Carousel1 from '../Selection/carousel/carousel1'
import Carousel2 from '../Selection/carousel/carousel2'

var a;
var h;
var b;
var f;


class body extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal1: false,
            modal2: false,
            modal3: false,
            header: null,
            footer: null,
            bannerType: null,
            bannerType2: null,
            button1: true,
            button2: true
        };

        this.toggle1 = this.toggle1.bind(this);
        this.toggle2 = this.toggle2.bind(this);
        this.toggle3 = this.toggle3.bind(this);
    }

    toggle1() {
        this.setState({
            modal1: !this.state.modal1
        });
    }
    toggle2() {
        this.setState({
            modal2: !this.state.modal2
        });
    }
    toggle3() {
        this.setState({
            modal3: !this.state.modal3
        })
    }
    render() {
        console.log(this.props);
        return (
            <main color="#ccc">
                <p className="title"><h4>Site Preview</h4></p>
                <Card>

                    <CardBody>
                        <div className="row">
                            <Col className="a relative no-padd">
                            
                                {
                                    this.state.header
                                }

                                {
                                    this.state.header === null ?
                                        <i className="material-icons padd" onClick={this.toggle1}>{this.props.buttonLabel}add_circle_outline <p>ADD HEADER COMPONENT</p></i>
                                        :
                                        <div>
                                            <Button className="remove-comp-btn"><i className="material-icons" onClick={() => this.setState({ header: null })}>delete</i>
                                            </Button>  <Button className="okay-comp-btn">
                                                <i className="material-icons" onClick={() => this.props.onExportHeader(this.state.header)}>check</i></Button></div>
                                }
                                <Modal isOpen={this.state.modal1} toggle={this.toggle1} className={this.props.className}>
                                    <ModalHeader toggle={this.toggle1}>Select Here</ModalHeader>
                                    <ModalBody>
                                        <Row><p className="p" onClick={() =>  this.setState({ header: <Header1 /> })}><Header1 /></p></Row>
                                        <Row><p className="p" onClick={() =>  this.setState({ header:<Header2 />})}><Header2 /></p></Row>
                                        <Row><p className="p" onClick={() => this.setState({header:<Header3 />})}><Header3 /></p></Row>
                                        <Row><p className="p" onClick={() => this.setState({header:<Header4 />})}><Header4 /></p></Row>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="primary" onClick={this.toggle1}>Select</Button>{' '}
                                        <Button color="secondary" onClick={this.toggle1}>Cancel</Button>

                                    </ModalFooter>
                                </Modal>
                            </Col>
                           
                        </div>
                        <div className="row ">
                            <Col className="b relative no-padd">
                                {
                                    this.state.bannerType
                                }
                                {
                                    this.state.bannerType === null ?
                                        <i className="material-icons btn btn-1" onClick={this.toggle3}>{this.props.buttonLabel}add_circle_outline<p>ADD BODY COMPONENTS</p></i>
                                        :
                                        <div>
                                            <Button className="remove-comp-btn"><i className="material-icons" onClick={() => this.setState({ bannerType: null })}>delete</i></Button>
                                            <Button className="okay-comp-btn"><i className="material-icons" onClick={() => this.props.onExportBanner(this.state.bannerType)}>check</i></Button>
                                        </div>
                                }
                                <Modal isOpen={this.state.modal3} toggle={this.toggle3} className={this.props.className}>
                                    <ModalHeader toggle={this.toggle3}>Select Here</ModalHeader>
                                    <ModalBody>
                                        {/* {
                                            this.state.bannerType2
                                        }
                                        {
                                            this.state.bannerType2=== null?
                                           <Row>
                                               <i className="material-icons prev" onClick={()=> this.setState({bannerType2: <Carousel2/>})} >chevron_left</i>
                                               <p className="p" onClick={() => this.setState({ bannerType: <Banner /> })}><Banner /></p>
                                               <i className="material-icons prev" onClick={()=> this.setState({bannerType2: <Minicarousel/>})} >chevron_right</i>
                                           </Row>
                                            
                                            :
                                            {}
                                            this.state.bannerType2=== <Minicarousel/>?
                                            <Row><p className="p" onClick={() => this.setState({ bannerType: <Minicarousel /> })}><Minicarousel /></p></Row>
                                            :
                                            {}
                                        
                                            
                                        }
                                        {
                                            this.state.bannerType2=
                                        } */}

                                        <Row><p className="p" onClick={() => this.setState({ bannerType: <Banner /> })}><Banner /></p></Row>
                                        <Row><p className="p" onClick={() => this.setState({ bannerType: <Minicarousel /> })}><Minicarousel /></p></Row>
                                        <Row><p className="p" onClick={() => this.setState({ bannerType: <Carousel1 /> })}><Carousel1 /></p></Row>
                                        <Row><p className="p" onClick={() => this.setState({ bannerType: <Carousel2 /> })}><Carousel2 /></p></Row>

                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="primary" onClick={this.toggle3}>Select</Button>{' '}
                                        <Button color="secondary" onClick={this.toggle3}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                            </Col>
                        </div>
                        <div className="row">
                            <Col className="a relative no-padd">
                                {

                                    this.state.footer
                                }
                                {
                                    this.state.footer === null ?
                                        <i className="material-icons" onClick={this.toggle2}>{this.props.buttonLabel}add_circle_outline <p>ADD FOOTER COMPONENT</p></i>
                                        :
                                        <div>
                                            <Button className="remove-comp-btn"><i className="material-icons" onClick={() => this.setState({ footer: null })}>delete</i></Button>
                                            <Button className="okay-comp-btn"><i className="material-icons" onClick={() => this.props.onExportFooter(this.state.footer)}>check</i></Button>
                                        </div>
                                }
                                <Modal isOpen={this.state.modal2} toggle={this.toggle2} className={this.props.className}>
                                    <ModalHeader toggle={this.toggle2}>Select Here</ModalHeader>
                                    <ModalBody>
                                        <Row><p onClick={() => this.setState({ footer: <Footer1 /> })}><Footer1 /></p></Row>
                                        <Row><p onClick={() => this.setState({ footer: <Footer2 /> })}><Footer2 /></p></Row>
                                        <Row><p onClick={() => this.setState({ footer: <Footer3 /> })}><Footer3 /></p></Row>
                                        <Row><p onClick={() => this.setState({ footer: <Footer4 /> })}><Footer4 /></p></Row>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="primary" onClick={this.toggle2}>Select</Button>{' '}
                                        <Button color="secondary" onClick={this.toggle2}>Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                            </Col>
                        </div>

                    </CardBody>
                </Card>
                <Link to="/preview">
                    <Button>
                        Preview
                </Button>
                </Link>
            </main>

        )
    }
}
function mapDispatchToProps(dispatch) {
    return {
        onExportHeader: (payload) => {
            console.log(payload);
            dispatch(exportHeader(payload));
        },
        onExportBanner: (payload)=>{
            console.log(payload);
            dispatch(exportBanner(payload));
        },
        onExportFooter: (payload)=>{
            console.log(payload);
            dispatch(exportFooter(payload));
        }
    }
}

function mapStateToProps(state) {
    return {
        headerData: state.global.header,
        bannerData: state.global.banner,
        footerData: state.global.footer,
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(body);