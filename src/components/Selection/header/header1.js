import React, { Component } from 'react';
import {connect} from 'react-redux'

import {
Collapse,
Navbar,
NavbarToggler,
NavbarBrand,
Nav,
NavItem,
NavLink,
UncontrolledDropdown,
DropdownToggle,
DropdownMenu,
DropdownItem
} from 'reactstrap';
import '../../../assets/header1.css';
import Cart from '../../../assets/5.jpg';


class Header1 extends Component {
constructor(props) {
super(props);
}
asideToggle(e) {
e.preventDefault();
document.body.classList.toggle('open-nav');
}
render() {
return (
<div>
	<Navbar light expand="md" style={{backgroundColor:"bisque"}}>
		<NavbarBrand href="">reactstrap</NavbarBrand>
		<NavbarToggler onClick={this.toggle} />
		<Collapse navbar>
			<Nav className="ml-auto" navbar>
				<NavItem>
				    <NavLink href="">Home</NavLink>
				        <div className="customDropdownMenu">
					        <div className="container">
						        <div className="row">
							        <div className="col-md-9">
								        <div className="row">
									        
									        <div className="col-md-4">
                                                <ul className="list-unstyled">
                                                    <li><a href="#">ALDO</a></li>
                                                    <li><a href="#">Allen Solly</a></li>
                                                    <li><a href="#">Armani Exchange</a></li>
                                                    <li><a href="#">Arrow</a></li>
                                                    <li><a href="#">BEBE</a></li>
                                                    <li><a href="#">BEVERLY HILLS POLO</a></li>
                                                    <li><a href="#">Bata</a></li>
                                                    <li><a href="#">Benetton</a></li>
                                                    <li><a href="#">CALL IT SPRING</a></li>
                                                    <li><a href="#">CHARLES &amp; KEITH</a></li>
                                                    <li><a href="#">Central</a></li>
                                                   
                                                </ul>
									        </div>
									        <div className="col-md-4">
                                                <ul className="list-unstyled">
                                                    <li><a href="">G - Star Raw</a></li>
                                                    <li><a href="">GUESS</a></li>
                                                    <li><a href="">Gant</a></li>
                                                    <li><a href="">Hidesign</a></li>
                                                    <li><a href="">Hugo Boss</a></li>
                                                    <li><a href="">INGLOT</a></li>
                                                    <li><a href="">IZOD</a></li>
                                                    <li><a href="">Jack &amp; Jones</a></li>
                                                    <li><a href="">LA SENZA</a></li>
                                                    <li><a href="">Levis</a></li>
                                                    
                                                </ul>
									        </div>
                                            <div className="col-md-4"><img src={Cart} alt="" /></div>
								        </div>
							        </div>
                                    <div className="col-md-3 menu-images">
                                        <img src="" alt="" className="d-block w-100" />
                                    </div>
						        </div>
					    </div>
				    </div>
				</NavItem>
				<NavItem>
					<NavLink href="">About Us</NavLink>
				</NavItem>
				<NavItem>
					<NavLink href="">Events</NavLink>
				</NavItem>
				<NavItem>
					<NavLink href="">Contact</NavLink>
				</NavItem>
				<UncontrolledDropdown nav inNavbar>
					<DropdownToggle nav caret>
						Options
					</DropdownToggle>
					<DropdownMenu right>
						<DropdownItem>
							Option 1
						</DropdownItem>
						<DropdownItem>
							Option 2
						</DropdownItem>
						<DropdownItem divider />
						<DropdownItem>
							Reset
						</DropdownItem>
					</DropdownMenu>
				</UncontrolledDropdown>
			</Nav>
		</Collapse>
	</Navbar>
</div>
);
}
}
export default connect (null)(Header1);