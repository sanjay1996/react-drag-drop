import React, { Component } from 'react'
import {connect} from 'react-redux'

class Header2 extends Component {
  render() {
    return (
      <div>
        <nav style={{zIndex:"0"}} className="navbar navbar-dark navbar-expand-md bg-success justify-content-between">
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-nav">
        <span className="navbar-toggler-icon"></span>
    </button>
    <div className="navbar-collapse collapse dual-nav w-100">
        <ul className="navbar-nav">
            <li className="nav-item active">
                <a className="nav-link pl-0" href="">Home <span className="sr-only">Home</span></a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="">Link</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="">Codeply</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="">Link</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="">Link</a>
            </li>
        </ul>
    </div>
    <a href="" className="navbar-brand mx-auto d-block text-center w-100">Brand</a>
    <div className="navbar-collapse collapse dual-nav w-100">
        <ul className="nav navbar-nav ml-auto">
        <li className="nav-item">
                <a className="nav-link" href="">Link</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="">Codeply</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" href="">Link</a>
            </li>
        </ul>
    </div>
</nav>
      </div>
    )
  }
}

export default connect(null)(Header2);