import React from 'react';
import {connect} from 'react-redux'


import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
  } from 'reactstrap';

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      disableDiv: false
    };
    this.disableDiv = this.disableDiv.bind(this);
    this.enableDiv = this.enableDiv.bind(this);
  }

  disableDiv() {
    this.setState({
       disableDiv:true
    });
  }

  enableDiv() {
    this.setState({
       disableDiv:false
    });
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div id="div" >
        <Navbar style={{zIndex:"0"}} light expand="md bg-light" >
          <NavbarBrand href="">reactstrap</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="">About Us</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="">Events</NavLink>
              </NavItem>
               <NavItem>
                <NavLink href="">Contact</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Options
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Option 1
                  </DropdownItem>
                  <DropdownItem>
                    Option 2
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    Reset
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export default connect (null)(Header);