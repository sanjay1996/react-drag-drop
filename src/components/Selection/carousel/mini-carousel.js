import React, { Component } from 'react'
import Slider from "react-slick";
import '../../../assets/mini-carousel.css';
import Img3 from '../../../assets/trip-stories-01.jpg';
import Img1 from '../../../assets/trip-stories-01.jpg';
import Img2 from '../../../assets/trip-stories-02.jpg';
import {connect} from 'react-redux'

 class MiniCarousel extends Component {
    render() {
        var settings = {        
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
                    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
};
        return (
          <Slider {...settings}>

           <div className="c-item  padd-zero zoom">
				<a className="card-comp white txt-overlay" href="#" title="trip-stories"> 
					<img src={Img1} alt="trip-stories" />
					<div className="card-comp-txt">
						<h2>Solo trip through Europe</h2>
						<p>A transcontinental ride across the famous biking trails of Europe, making new friends and memories all along the way.</p>
						<button className="custom-btn bor arrow-r">Find Out More</button>
					</div>
				</a>  
			</div>

            <div className="c-item  padd-zero zoom">
				<a className="card-comp white txt-overlay" href="#" title="trip-stories">
					<img src={Img2} alt="trip-stories" />
					<div className="card-comp-txt">
					<h2>Weekend GT Ride</h2>
					<p>A weekend ride on my Continental GT around the outskirts of Miami.</p>
						<button className="custom-btn bor arrow-r">Find Out More</button>
					</div>
				</a>  
			</div>
            
            <div className="c-item padd-zero zoom">
				<a className="card-comp white txt-overlay" href="#" title="trip-stories">
					<img src={Img3} alt="trip-stories" />
					<div className="card-comp-txt">
					<h2>#Reunion in the hills</h2>
					<p>Riding out to the hills of Manali to meet up with some of my riding mates from back in the day.</p>
						<button className="custom-btn bor arrow-r">Find Out More</button>
					</div>
				</a>
			</div>

            <div className="c-item  padd-zero zoom">
				<a className="card-comp white txt-overlay" href="#" title="trip-stories">
					<img src={Img2} alt="trip-stories" />
					<div className="card-comp-txt">
					<h2>Weekend GT Ride</h2>
					<p>A weekend ride on my Continental GT around the outskirts of Miami.</p>
						<button className="custom-btn bor arrow-r">Find Out More</button>
					</div>
				</a>  
			</div>

             <div className="c-item  padd-zero zoom">
				<a className="card-comp white txt-overlay" href="#" title="trip-stories"> 
					<img src={Img1} alt="trip-stories" />
					<div className="card-comp-txt">
						<h2>Solo trip through Europe</h2>
						<p>A transcontinental ride across the famous biking trails of Europe, making new friends and memories all along the way.</p>
						<button className="custom-btn bor arrow-r">Find Out More</button>
					</div>
				</a>  
			</div>
        
           
          </Slider>
        );
      }
    }
    export default connect(null)(MiniCarousel);