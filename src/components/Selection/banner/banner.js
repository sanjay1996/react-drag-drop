import React, { Component } from 'react'
import Bike from '../../../assets/showcase.jpg';
import '../../../assets/banner.css';
import {connect} from 'react-redux'

class Banner extends Component {
  render() {
    return (
      <div>
        <section>
		<div className="re-banner mobi-banr-heading-only" style={{zIndex:"0"}}> 
			<div className="banner-inner-img">
				<picture>
				    <source srcset="#" media="(max-width: 767px)" />
				    <img srcset={Bike} alt="Royal Enfield" />
				</picture>
		    </div> 
		    <div className="banner-inner-txt">
				<div className="disp-tbl">
					<div className="disp-tbl-cel"> 
		    			<div className="container-fluid">
				    		<div className="txt-box">
						    	<h1>Royal Enfield</h1>
								<p>Royal Enfield showcased two of its custom-built motorcycles, Dirty Duck and Mo'Powa at this year's edition of the French motorcycling festival, Wheels and Waves.</p>
								<a className="custom-btn bor arrow-r clr-w" href="#">Know More</a>
							</div>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</section>

	
      </div>
    )
  }
}
export default connect(null)(Banner);