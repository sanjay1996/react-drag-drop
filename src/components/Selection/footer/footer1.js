import React, { Component } from 'react';
import '../../../assets/footer1.css';
import {connect} from 'react-redux'

class Footer1 extends Component {
    render() {
        return (
            <footer className="row footer-arya" style={{zIndex:"0"}}>
            <div className="container">
                <div className="row">
                    <div className="col-md-9">
                        <div className="row">
                              <div className="col-sm-6 col-md-4">
                                 <h4>Categories</h4> 
                                 <ul className="footer-linke">        
                                     <li><a href="#">Apparel &amp; Accessories</a></li>
                                     <li><a href="#">Food &amp; Beverage</a></li>
                                     <li><a href="#">Mobile &amp; Electronics</a></li>
                                     <li><a href="#">Health &amp; Wellness</a></li>
                                     <li><a href="#">Movie &amp; Magazines</a></li>
                                     <li><a href="#">Cabs &amp; Travel</a></li>
                                     <li><a href="#">e-Com &amp; Online</a></li>
                                     <li><a href="#">Grocery &amp; Home Needs</a></li>
                                 </ul>
                              </div>
                            <div className="col-sm-6 col-md-4">
                                 <h4>Quick Links</h4> 
                                 <ul className="footer-linke">         
                                     <li><a href="#">Login</a></li>
                                     <li><a href="#">Wishlist</a></li>
                                     <li><a href="#">My Cart</a></li>
                                     <li><a href="#">Checkout</a></li>
                                     <li><a href="#">Terms of Use</a></li>
                                     <li><a href="#">Privacy Policy</a></li>
                                 </ul>
                              </div>
                              <div className="col-sm-6 col-md-4">
                                 <h4>Company</h4> 
                                 <ul className="footer-linke">
                                     <li><a href="#">About Us</a></li>     
                                     <li><a href="#">News</a></li>
                                     <li><a href="#">Press Kit</a></li>
                                     <li><a href="#">FAQs</a></li>
                                     <li><a href="#">Get in Touch</a></li>
                                 </ul>
                              </div>
                          </div>
                      </div>
                      
                      <div className="col-md-3">
                          <div className="row">
                              <div className="col-sm-12">
                                <h4>Download the App </h4>
                                <ul className="list-inline row">         
                                     <li className="list-inline-item m-0 col-6 pl-0 p-1"><a href="#"><img src="" /></a></li>
                                     <li className="list-inline-item m-0 col-6 pl-0 p-1"><a href="#"><img src="" /></a></li>
                                 </ul>
                              </div>
                              
                              <div className="col-sm-12">
                                <h4>100% Secure Payment</h4>
                                <ul className="payment-logos accordion-items">
                                    <li className="visa">Visa</li>
                                    <li className="mastercard">MasterCard</li>
                                    <li className="amex">American Express</li>
                                </ul>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-12 text-center text-md-right">
                        <ul className="list-inline footer-social">         
                            <li className="list-inline-item"><a href="#"><i className="fab fa-facebook-square"></i></a></li>
                            <li className="list-inline-item"><a href="#"><i className="fab fa-linkedin"></i></a></li>
                            <li className="list-inline-item"><a href="#"><i className="fab fa-google-plus-square"></i></a></li>
                            <li className="list-inline-item"><a href="#"><i className="fab fa-twitter-square"></i></a></li>
                            <li className="list-inline-item"><a href="#"><i className="fab fa-instagram"></i></a></li> 
                         </ul>
                    </div>
                  </div>
            </div>
        </footer>
        );
    }
}
export default connect (null)(Footer1);