import React, { Component } from 'react'
import '../../../assets/footer3.css';
import {connect} from 'react-redux'

class Footer3 extends Component {
  render() {
    return (
      <div>
         <footer className="footer" style={{zIndex:"0"}}>

<div className="footer-middle">
    <div className="container-fluid">
        <div className="row">
            <div className="col-sm-4">
                <h3>life skill resources</h3>
                <ul className="footer-menu footer-menu-50 cls">
                    <li>
                        <a href="index.html">home</a>
                    </li>
                    <li>
                        <a href="about.html">about</a>
                    </li>
                    <li>
                        <a href="events.html">events</a>
                    </li>
                    <li>
                        <a href="free-workshops.html">free workshops</a>
                    </li>
                    <li>
                        <a href="speakers.html">speakers</a>
                    </li>
                    <li>
                        <a href="training.html">training</a>
                    </li>
                    <li>
                        <a href="kids-resources.html">kids resources</a>
                    </li>
                    <li>
                        <a href="blog.html">blog</a>
                    </li>
                    <li>
                        <a href="gallery.html">gallery</a>
                    </li>
                    <li>
                        <a href=""  data-toggle="modal" data-target="#join-us-modal">join us</a>
                    </li>
                    <li>
                        <a href="contact.html">contact</a>
                    </li>
                </ul>
                <p className="text-left payment-icons">
                   
                </p>
            </div>
            <div className="col-sm-4">
                <h3>upcoming events</h3>
                <ul className="footer-menu">
                    <li>
                        <a href="#">
                            <p>Kids Summer Programme – Delhi</p>
                            <span>May 14</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>Super - Mom summer Programme</p>
                            <span>June 21</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>Story telling</p>
                            <span>July 09</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div className="col-sm-4">
                <h3>reach us</h3>
                <p>
                    <strong>Delhi</strong><br />31-B, Pocket - L, Sheikh Sarai-2, New Delhi - 110017
                </p>
                <p>
                    <strong>Faridabad</strong><br />SF 6, 2nd Floor Vishnu Palace, Ajronda Chowk, Faridabad -121001
                </p>
                <p>
                    <strong>Gurgaon</strong><br />The Close North, Nirvana Country, Sector 50, Gurgaon-122018
                </p>
            </div>
        </div>
    </div>
</div>

<div className="footer-bottom">
    <div className="container-fluid">
        <div className="row">
            <div className="col-sm-6">
                <p>© 2018 Life Skill Resources | All rights reserved.</p>
            </div>
            <div className="col-sm-6 text-right">
                <p><strong>Follow us:</strong>
                    <a href="#"><i className="fa fa-facebook-official" aria-hidden="true"></i></a>
                    <a href="#"><i className="fa fa-twitter-square" aria-hidden="true"></i></a>
                    <a href="#"><i className="fa fa-linkedin-square" aria-hidden="true"></i></a>
                </p>
            </div>
        </div>
    </div>
</div>

</footer>
      </div>
    )
  }
}

export default connect(null)(Footer3);