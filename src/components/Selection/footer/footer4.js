import React, { Component } from 'react'
import '../../../assets/footer4.css';
import {connect} from 'react-redux'

class Footer4 extends Component {
  render() {
    return (
      <div>
        <footer className="footer" style={{zIndex:"0"}}>
    	<div className="container-fluid">
    		<div className="row">
    			<div className="col-xl-6 col-md-6 col-sm-12">
    				<div className="subscribe-to-newsletter">
    					<p>Subscribe to Newsletter</p>
    					<div className="subscribe-box"> 
    						<input className="input-field" type="text" name="" placeholder="Your email ID" />
    						<button className="custom-btn">Submit</button> 
    					</div>
    				</div>
    			</div>
    			<div className="col-xl-6 col-md-6 col-sm-12 text-right">
    				<div className="social-links">
    					<p>Join the Conversation</p> 
    					<ul> 
    						<li><a href="" title="facebook"><span className="icon-fb"></span></a></li>
    						<li><a href="" title="twitter"><span className="icon-tw"></span></a></li>
    						<li><a href="" title="youtube"><span className="icon-yt"></span></a></li>
    						<li><a href="" title="instagram"><span className="icon-ig"></span></a></li>
    					</ul> 
    				</div>
    			</div>
    		</div>
    	 
    		<div className="row"> 
    			<div className="col-xl col-md-3 col-sm-12">
    				<div className="list-of-links">
    					<h3><span className="icon-arrow-down"></span> Motorcycles</h3>
    					<ul className="nav-list">
    						<li><a href="continental-gt-models.html">Continental GT</a></li>
    						<li className="nav-list-children">
    							<a>Thunderbird<span className="icon-arrow-down"></span></a>
    							<ul>
    								<li><a href="">Thunderbird 500</a></li>
    								<li><a href="">Thunderbird 350</a></li>
    							</ul>
    						</li>
    						<li><a href="">Himalayan</a></li>
    						<li className="nav-list-children">
    							<a>Classic<span className="icon-arrow-down"></span></a>
    							<ul>
    								<li><a href="">Classic Squadron Blue</a></li>
    								<li><a href="">Classic Desert Storm</a></li>
    								<li><a href="">Classic Battle Green</a></li>
    								<li><a href="">Classic Chrome</a></li>
    								<li><a href="">Classic 500</a></li>
    								<li><a href="">Classic 350</a></li>
    								<li><a href="">Classic Stealth Black</a></li>
    								<li><a href="">Classic Gunmetal Grey</a></li>
    							</ul>
    						</li>
    						<li className="nav-list-children">
    							<a>Bullet<span className="icon-arrow-down"></span></a>
    							<ul>
    								<li><a href="">Bullet 500</a></li>
    								<li><a href="">Bullet 350</a></li>
    								<li><a href="">Bullet Es</a></li>
    							</ul>
    						</li>
    					</ul>
    				</div>
    			</div>
    			<div className="col-xl col-md-3 col-sm-12">
    				<div className="list-of-links">
    					<h3><span className="icon-arrow-down"></span> Rides &amp; Events</h3>
    					<ul className="nav-list">
    						<li><a href="">Create a Ride</a></li>
    						<li><a href="">Rides</a></li>
    						<li><a href="">Events</a></li>
    					</ul>
    				</div>
    			</div>
    			<div className="col-xl col-md-3 col-sm-12">
    				<div className="list-of-links">
    					<h3><span className="icon-arrow-down"></span> Support</h3>
    					<ul className="nav-list">
    						<li><a href="">Service Center</a></li>
    						<li><a href="">Stores</a></li>
    						<li><a href="">Owner Manual</a></li>
    						<li><a href="">Contact Us</a></li>
    						<li><a href="">Book A Test Ride</a></li>
    					</ul>
    				</div>
    			</div>
    			<div className="col-xl col-md-3 col-sm-12">
    				<div className="list-of-links">
    					<h3><span className="icon-arrow-down"></span> About Us</h3>
    					<ul className="nav-list">
    						<li><a href="">About Royal Enfield</a></li>
    						<li><a href="">About Eicher Motors</a></li>
    						<li><a href="">History</a></li>
    						<li><a href="">Factory Tour</a></li>
    					</ul>
    				</div>
    			</div>
    			<div className="col-xl col-md-3 col-sm-12">
    				<div className="list-of-links">
    					<h3><span className="icon-arrow-down"></span> Media</h3>
    					<ul className="nav-list">
    						<li><a href="">Campaigns</a></li>
    						<li><a href="">News</a></li>
    						<li><a href="">Media Kit</a></li>
    						<li><a href="">Publications</a></li>
    					</ul>
    				</div>  
    			</div>
    			<div className="col-xl col-md-3 col-sm-12">
    				<div className="list-of-links">
    					<h3><span className="icon-arrow-down"></span> Careers</h3>
    					<ul className="nav-list">
    						<li><a href="">Our Culture</a></li>
    						<li><a href="">Job Openings</a></li>
    					</ul>
    				</div>
    			</div>
    			<div className="col-xl col-md-3 col-sm-12">
    				<div className="list-of-links">
    					<h3><span className="icon-arrow-down"></span> Our World</h3>
    					<ul className="nav-list">
    						<li><a href="">Forum</a></li>
    						<li><a href="">Trip Stories</a></li>
    						<li><a href="">Social Hub</a></li>
    					</ul>
    				</div>
    			</div>
    		</div>
    
    		<div className="row">
    			<div className="col-md-4">
    				<div className="copyright">
    					<p>&copy; 2018. Royal Enfield.</p>
    				</div>
    			</div>
    			<div className="col-md-8 text-right"> 
    				<ul className="extra-links">
    					<li><a href="">Cookies</a></li>
    					<li><a href="">Imprint</a></li>
    					<li><a href="">Data Protection</a></li>
    					<li><a href="">Privacy</a></li> 
    					<li><a href="">Legal Notices</a></li>
    				</ul>
    			</div>
    		</div>
    	</div> 
    </footer>
    
      </div>
    )
  }
}

export default connect(null)(Footer4);