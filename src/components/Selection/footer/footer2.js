import React, { Component } from 'react'
import '../../../assets/footer2.css';
import {connect} from 'react-redux'

class Footer2 extends Component {
  render() {
    return (
      <div>
         <footer>
			<div className="container">
				<div className="row">
					<div className="col-sm-3">
						<form>
							<label className="text-uppercase">sign up for the newsletter</label>
							<div className="relative">
								<input type="text" className="form-control" placeholder="Enter Your Email..." />
								{/* <button className="btn" type="button">
								<i className="fa fa-angle-right" aria-hidden="true"></i>
								</button> */}
							</div>
						</form>
					</div>
					<div className="col-sm-3">
						<label className="text-uppercase">quick links</label>
						<ul>
							<li className="nav-item">
								<a className="nav-link" href="#">About Us</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="#">Why We Differ</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="#">Previous</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="#">Get In Touch</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="#">Privacy Policy</a>
							</li>
						</ul>
					</div>
					<div className="col-sm-3">
						<label className="text-uppercase">follow us on</label>
						<ul>
							<li className="nav-item">
								<a className="nav-link" href="#" title="Facebook" alt="Facebook" target="_blank">Facebook</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="#" title="Meetup" alt="Meetup" target="_blank">Meetup</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" href="#" title="Youtube" alt="Youtube" target="_blank">Youtube</a>
							</li>
						</ul>
					</div>
					<div className="col-sm-3">
						<label className="text-uppercase">contact us</label>
						<p>
							<a href="#">info@sailingthemed.com</a>
						</p>
						<p>
							<a href="#">+44 7459 632 427</a>
						</p>
					</div>
				</div>
			</div>
		</footer>
      </div>
    )
  }
}
export default connect(null)(Footer2);