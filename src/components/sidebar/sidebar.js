import React from 'react';
import '../sidebar/Sidebar.css'
import { ListGroup, ListGroupItem, NavLink, Collapse, Nav,  } from 'reactstrap';
import {connect} from 'react-redux'

class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = { collapse: false };
        
      }
      toggle() {
        this.setState({ collapse: !this.state.collapse });
      }

    render() {
        return (
            
            <aside className="aside">
                <div className="aside-inner">
                    <ListGroup color="light">
                        <ListGroupItem className="items" >
                            <NavLink style={{color:"lightslategrey"}} href="/">
                                 Create a Website
                            </NavLink>
                        </ListGroupItem>
                        <ListGroupItem className="items" onClick={this.toggle}>
                            <NavLink>
                            Edit a website
                            </NavLink>
                            <Collapse className="subitems" isOpen={this.state.collapse}>
                                working
                            </Collapse>
                        </ListGroupItem >
                        
                    </ListGroup>
                </div>
            </aside>
        );
    }
}

export default connect(null)(Sidebar);