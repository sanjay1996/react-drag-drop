import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/header/header';
import Sidebar from './components/sidebar/sidebar';
import Body from './components/body/body'
import { container } from 'reactstrap';
import {Provider, connect} from 'react-redux';
import store from './store';

class App extends Component {
  render() {
    return (
    
      <div className="App">
        <header className="header">
          <Header />
        </header>
        <div className="app-body">
          <Sidebar />
          <Body />
        </div>
       
      </div>
    
    );
  }
}

export default connect(null)(App);
