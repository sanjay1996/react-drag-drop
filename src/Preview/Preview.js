import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom'


class Preview extends Component {
  render() {
    return (
      <div>
        <div>
          {this.props.headerData}
          {this.props.bannerData}
          {this.props.footerData}
        </div>
        <div>
          <Link to='/'>
            <Button>
              Back to home
            </Button>
          </Link>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    headerData: state.global.header,
    bannerData: state.global.banner,
    footerData: state.global.footer,
  }
}


export default connect(mapStateToProps)(Preview);
