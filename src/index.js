import React from 'react';
import ReactDOM from 'react-dom';
import ABootstrap from 'react-bootstrap';
import './assets/bootstrap.min.css';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

//import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Preview from './Preview/Preview'
import { Provider, connect } from 'react-redux';
import store from './store';


ReactDOM.render((
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route path="/Preview" name="Preview" component={Preview} />
                <Route path="/" name="home" exact component={App} />

            </Switch>
        </BrowserRouter>
    </Provider>
), document.getElementById('root'));
registerServiceWorker();
