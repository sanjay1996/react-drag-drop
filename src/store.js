import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './redux/reducers'
import { logger } from 'redux-logger'

const initialState = {};

// const middleware = [thunk, logger];
const devTools = window.devToolsExtension ? window.devToolsExtension() : f => f;
const enhancers = compose(applyMiddleware(thunk, logger), devTools)
const store = createStore(rootReducer, enhancers);

export default store;
