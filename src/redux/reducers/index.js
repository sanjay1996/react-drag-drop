import { combineReducers } from 'redux';
import maty from './maty'
import walia from "./walia";
import global from "./global"

export default combineReducers({
    a: maty,
    w: walia,
    global,
});
