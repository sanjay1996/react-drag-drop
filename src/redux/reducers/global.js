import { EXPORT_HEADER, EXPORT_FOOTER, EXPORT_BANNER } from '../actionType/global';

const initialState = {
    items: [],
    header: null,
    banner: null,
    footer: null,
};

export default function GlobalReducer(state = initialState, action) {
    switch (action.type) {
        case EXPORT_HEADER:
            return {
                ...state, header: action.payload
            };
        case EXPORT_BANNER:
            return {
                ...state, banner: action.payload
            };
        case EXPORT_FOOTER:
            return {
                ...state, footer: action.payload
            };
        default:
            return state;
    }
}