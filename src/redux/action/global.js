import { EXPORT_HEADER, EXPORT_BANNER, EXPORT_FOOTER } from '../actionType/global';

export function exportHeader(payload) {
    return {
        type: EXPORT_HEADER,
        payload
    }
} 

export function exportBanner(payload){
    return{
        type: EXPORT_BANNER,
        payload
    }
}
export function exportFooter(payload){
    return{
        type: EXPORT_FOOTER,
        payload
    }
}